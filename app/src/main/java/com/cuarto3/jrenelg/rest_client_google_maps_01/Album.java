package com.cuarto3.jrenelg.rest_client_google_maps_01;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by jrenelg on 2/15/18.
 */

public class Album {
    @JsonProperty("userId")
    private long userId;
    @JsonProperty("id")
    private long id;
    @JsonProperty("title")
    private String title;

}