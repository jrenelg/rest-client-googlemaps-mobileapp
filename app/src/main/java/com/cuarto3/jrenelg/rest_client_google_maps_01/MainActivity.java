package com.cuarto3.jrenelg.rest_client_google_maps_01;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;




public class MainActivity extends AppCompatActivity {
    public HttpStatus statusCode;
    public Album albumReturned;
    class RESTTask extends AsyncTask<String, Void, ResponseEntity<Album>>{
        protected ResponseEntity<Album> doInBackground(String... uri){
            final String url = uri[0];
            RestTemplate restTemplate = new RestTemplate();
            try {
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                /*headers.set("name","value");
                String auth = "user:password";
                String encodedAuth = Base64.encodeToString(auth.getBytes(),Base64.DEFAULT);
                String authHeader = "Basic" + new String(encodedAuth);
                headers.set("Authorization",authHeader);*/
                HttpEntity<String> entity = new HttpEntity<String>(headers);
                ResponseEntity<Album> response = restTemplate.exchange(url,HttpMethod.GET,entity,Album.class);
                //Album body = response.getBody();
                return response;
            }catch (Exception ex){
                String message = ex.getMessage();
                ex.printStackTrace();
                return null;
            }

        }
        protected void onPostExecute(ResponseEntity<Album> result){
            statusCode = result.getStatusCode();
            albumReturned = result.getBody();
            TextView txt = (TextView) findViewById(R.id.responseText);
            ObjectMapper mapper = new ObjectMapper();
            try {
                txt.setText("status:"+statusCode.toString()+"|response:"+mapper.writeValueAsString(albumReturned));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            Toast.makeText(getApplicationContext(), "Data retrieved", Toast.LENGTH_LONG).show();
        }
    }



    public void onClickGetLocations(View view){
        final String uri = "https://jsonplaceholder.typicode.com/albums/2";
        new RESTTask().execute(uri);


    }

    public void onClickViewInMap(View view){
        startActivity(new Intent(MainActivity.this, MapsActivity.class));
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
